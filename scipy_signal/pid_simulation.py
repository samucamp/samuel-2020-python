import numpy as np
import sympy as sy
import matplotlib.pyplot as plt
from scipy import signal


def lti_to_sympy(lsys, symplify=True):
    """ Convert Scipy's LTI instance to Sympy expression """
    s = sy.Symbol('s')
    G = sy.Poly(lsys.num, s) / sy.Poly(lsys.den, s)
    return sy.simplify(G) if symplify else G


def sympy_to_lti(xpr, s=sy.Symbol('s')):
    """ Convert Sympy transfer function polynomial to Scipy LTI """
    num, den = sy.simplify(xpr).as_numer_denom()  # expressions
    p_num_den = sy.poly(num, s), sy.poly(den, s)  # polynomials
    c_num_den = [sy.expand(p).all_coeffs()
                 for p in p_num_den]  # coefficients
    l_num, l_den = [sy.lambdify((), c)()
                    for c in c_num_den]  # convert to floats
    return signal.lti(l_num, l_den)


def step(lti):
    t, y = signal.step(lti)
    plt.plot(t, y)
    plt.xlabel('Time [s]')
    plt.ylabel('Amplitude')
    plt.title('Step response for 1. Order Lowpass')
    plt.grid()
    plt.show()


# Sample systems:
G = signal.lti([0.56], [0.5, 1])
C = signal.lti([2.16, 0.374], [1, 0])

# convert to Sympy:
print("Converting to sympy")
Gs, Cs = lti_to_sympy(G), lti_to_sympy(C)


print("Multiplying Systems")
# make sure polynomials are canceled and expanded
CGs = sy.simplify(Gs*Cs).expand()

print("Closing the loop")
CGs = sy.simplify(CGs / (1+CGs)).expand()

print("Back to LTI")
CGs = sympy_to_lti(CGs)
print(CGs)

step(CGs)
