from scipy import signal
import matplotlib.pyplot as plt

from models.step_tfc import StepResponseTfc
from models.step_lti import StepResponseLti

teste1 = StepResponseTfc([1], [1, 2, 3])
teste2 = StepResponseTfc([1, 0], [1, 2, 3])
teste3 = StepResponseTfc([1, 2, 3], [1, 2, 3, 4])
teste4 = StepResponseTfc([1], [1, 1])
teste5 = StepResponseTfc([1], [1, 1, 1])

teste1.plotStep()
teste2.plotStep()
teste3.plotStep()
teste4.plotStep()
teste5.plotStep()

StepResponseTfc.showGraph()
