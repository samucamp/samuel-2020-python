from scipy import signal

numerador = [1, 3, 2]
denominador = [1, 2, 1]


# continuo
Tfc = signal.TransferFunction(numerador, denominador)
print(Tfc)

lti = signal.lti([1.0], [1.0, 1.0])
print(lti)

# discreto
Tfd = signal.TransferFunction(numerador, denominador, dt=0.1)
print(Tfd)


# state space
print(Tfc.to_ss())
print(Tfd.to_ss())


# transfer function
print(Tfc.to_tf())
print(Tfd.to_tf())


# zpk (representação zero pole gain)
print(Tfc.to_zpk())
print(Tfd.to_zpk())
