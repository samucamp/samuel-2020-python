from scipy import signal
import matplotlib.pyplot as plt


class StepResponseLti:
    def __init__(self, numerador, denominador):
        self.numerador = numerador
        self.denominador = denominador

    def step(self):
        lti = signal.lti(self.numerador, self.denominador)
        t, y = signal.step(lti)
        plt.plot(t, y)
        plt.xlabel('Time [s]')
        plt.ylabel('Amplitude')
        plt.title('Step response for 1. Order Lowpass')
        plt.grid()
        plt.show()
