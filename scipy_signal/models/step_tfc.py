from scipy import signal
import matplotlib.pyplot as plt


class StepResponseTfc:
    def __init__(self, numerador, denominador):
        self.numerador = numerador
        self.denominador = denominador

    def plotStep(self):
        Tfc = signal.TransferFunction(self.numerador, self.denominador)
        t, y = signal.step(Tfc)
        plt.plot(t, y)
        plt.xlabel('Time [s]')
        plt.ylabel('Amplitude')
        plt.title('Step response for 1. Order Lowpass')
        plt.grid()

    @staticmethod
    def showGraph():
        plt.show()
