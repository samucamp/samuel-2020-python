import numpy as np

x = np.arange(6)
print(x)

# SHAPE RESHAPE array as matrix
y = x.reshape((3, 2))
print(y)

# CONVERT ARRAY

x = y.reshape((1, 6))
print(x)
