import numpy as np
import matplotlib.pyplot as plt


# read
img1 = plt.imread('../../Imagens/img_car.jpg')
plt.imshow(img1)


# display
plt.axis('off')
plt.title('Car')
plt.show()


# save
plt.imsave('./teste.jpg', img1)
