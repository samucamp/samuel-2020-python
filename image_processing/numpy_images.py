import numpy as np
import matplotlib.pyplot as plt


# read
img1 = plt.imread('../../Imagens/img_car.jpg')
plt.imshow(img1)


print(img1)
print(type(img1))
print(img1.shape)
print(img1.ndim)
print(img1.size)
print(img1.nbytes)

plt.show()
