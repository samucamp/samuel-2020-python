# OBS.: PARA EXECUTAR digitar no terminal
# python .\area_circulo.py

from math import pi
import sys
import errno


# CORES PARA PRINT DE ERRO
class TerminalColor:  # fugindo do escopo global
    ERRO = '\033[91m'
    NORMAL = '\033[0m'


# import sys pra liberar argv
print(sys.argv[0])  # primeiro argumento .\area_circulo.py


def circulo(raio):
    return pi * float(raio)**2


def help(script):
    print("É necessário informar o raio do círculo.")
    print("Sintaxe: {} <raio>".format(script))


if __name__ == '__main__':
    if len(sys.argv) < 2 or len(sys.argv) > 2:
        print(TerminalColor.ERRO +
              "Quantidade de argumento errada" + TerminalColor.NORMAL)
        help(sys.argv[0][2:])
        # sys.exit(1)  # sai do programa com erro (1=erro)
        sys.exit(errno.EPERM)  # erro

    elif not sys.argv[1].isnumeric():
        print(TerminalColor.ERRO + "Argumento inválido" + TerminalColor.NORMAL)
        help(sys.argv[0][2:])
        # sys.exit(1)  # sai do programa com erro (1=erro)
        sys.exit(errno.EINVAL)  # argumento inválido´

    else:  # else não necessário com uso do sys.exit
        # raio = input('Informe o raio: ')
        raio = sys.argv[1]  # segundo argumento (input)
        area = circulo(raio)
        print('Área do circulo', area)
