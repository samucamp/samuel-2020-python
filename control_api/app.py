from flask import Flask, request
from models.step_response import stepResponse as step
import json

app = Flask(__name__)


@app.route('/')
def home():
    return '<h1>Home</h1>'


@app.route("/json")
def control_api():
    y = json.loads(request.data)
    x = step(y["numerador"], y["denominador"])
    return x


if __name__ == "__main__":
    app.run(debug=True)
    # debug=True is set to allow possible errors to appear on the web page.
    # IMPORTANT POINT: This shall never be used in production environment
