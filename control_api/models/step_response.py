from scipy import signal
import json


def stepResponse(numerador, denominador):
    Tfc = signal.TransferFunction(numerador, denominador)
    t, y = signal.step(Tfc)

    # a Python object (dict):
    response = {
        "numerador": numerador,
        "denominador": denominador,
        "yaxis": list(y),
        "xaxis": list(t)
    }

    # convert into JSON:
    json_dump = json.dumps(response)
    return json_dump
