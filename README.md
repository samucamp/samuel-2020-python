# SAMUEL 2020 - Python

Aprendendo a linguagem Python de diversas fontes.

<br>

# Conteúdos
1. [Instalando Python no UBUNTU](#python)
2. [Gerenciamento de Pacotes com PIP](#pip)
3. [Referências](#ref)

<br>

# Instalando Python no UBUNTU
<a name="python"></a>

Mais informações sobre uso do Ubuntu em: [SAMUEL 2020 - Linux Ubuntu](https://gitlab.com/samucamp/samuel-2020-linux-ubuntu)

Instalando python 3.8
```bash
sudo apt-get install python3.8
```

Instalando a versão mais atual de pip
```bash
/usr/bin/python3.8 -m pip install -U pip --user
/usr/bin/python3.8 -m pip install --user --upgrade pip 
```

Instalações para VSCode
```bash
/usr/bin/python3.8 -m pip install -U flake8 --user
/usr/bin/python3.8 -m pip install -U autopep8 --user
/usr/bin/python3.8 -m pip install -U pylint --user
```

Bibliotecas adicionais
```bash
/usr/bin/python3.8 -m pip install -U matplotlib --user
/usr/bin/python3.8 -m pip install -U numpy --user
/usr/bin/python3.8 -m pip install -U scipy --user
/usr/bin/python3.8 -m pip install -U pytest --user
```

Para funcionamento correto do matplotlib
```bash
/usr/bin/python3.8 -m pip install -U Pillow --user
sudo apt-get install python3-tk
```

Ainda no VSCode, talvez seja necessário adicionar em settings.json

Assim a versão 3.8 será a considerada na interpretação
```json
{
    "python.pythonPath": "/usr/bin/python3.8"
    "code-runner.executorMap": {
        "python": "$pythonPath -u $fullFileName"
    }
}
```

Verificando versões
```bash
python --version
python3 --version
python3.8 --version
pip --version
pip3 --version

```

<br>

# Gerenciamento de Pacotes com PIP
<a name="pip"></a>

Verificando versão
```sh
pip3 --version
```

Verificando paths (sys.path) <code>Verificar python3, python3.8</code>
```sh
python -m site
```

Instalando, importando e desinstalando pacotes. Django de exemplo.
```sh
pip3 install django (global)
pip3 install --user django (usuário)
python -c 'import django; print(django)'
pip3 uninstall django
python -m django (verificando instalado)
```

Verificando instalações
```sh
pip3 list
pip3 freeze
```

Criando arquivo com lista de dependências, instalando no ambiente e append
```sh
pip3 freeze > requirements.txt
pip3 install -r requirements.txt
echo 'django==2.2.4' >> requirements.txt (append)
```

Detectando bibliotecas desatualizadas e atualizando
```sh
pip3 list --outdated
pip3 install --upgrade [package]
pip3 install --upgrade setuptools
```

Ambiente virtual/isolado. Módulo VENV. Ideal para utilizar um arquivo requirements.txt e instalar dentro do VENV. Idéia: ter projetos independentes com bibliotecas independentes.

A sequência de comandos abaixo cria uma pasta para o projeto, instala os pacotes necessários para utilizar o virtualenv e inicializa o ambiente virutal .venv (oculto) com o interpretador python. Mostra o sys.path do ambiente e atualiza a versão dos pacotes pre-instalados, incluindo pacotes e versões em requirements.txt.

```sh
cd ~/...../pasta_isolada
sudo apt-get install python3-venv
python3 -m pip install --user virtualenv 
python3 -m venv .venv
source .venv/bin/activate
python3 -m site
echo $VIRTUAL_ENV
python3 -c 'import sys; print("\n".join(sys.path))'
pip list
pip install --upgrade pip
pip install --upgrade setuptools
pip install numpy
pip list
pip freeze > requirements.txt
```

Para desativar e sair do ambiente venv
```
deactivate
```

Procurando pacotes e instalando versão específica
```sh
pip search [package]    
pip search requests
pip install requests==2.19.1
```

<br>

# Referências
<a name="ref"></a>

> https://www.w3schools.com/python/default.asp

> https://www.udemy.com/course/curso-python-3-completo/

> https://www.udemy.com/course/complete-python-raspberry-pi-and-iot-bootcamp/

> https://docs.python.org/3/tutorial/venv.html