class RGB:
    def __init__(self):
        # durante iteração, pega do ultimo pro primeiro com .pop então usa [::-1] para pegar do primeiro
        self.cores = ['red', 'green', 'blue'][::-1]

    # objeto iteravel
    def __iter__(self):
        return self

    # iterator
    def __next__(self):
        try:
            return self.cores.pop()
        except IndexError:
            raise StopIteration()


if __name__ == '__main__':
    cores = RGB()
    print(next(cores))
    print(next(cores))
    print(next(cores))

    try:
        print(next(cores))
    except StopIteration:
        print('Acabou!')

    # para iterar deve utilizar __iter__
    for cor in RGB():
        print(cor)
