class Carro:
    # Construtor
    # Sempre executa __init__ quando é criado uma instância
    def __init__(self, velocidade_maxima):
        self.velocidade_maxima = velocidade_maxima
        self.velocidade_atual = 0

    # método acelerar
    def acelerar(self, delta=5):
        maxima = self.velocidade_maxima
        nova = self.velocidade_atual + delta
        self.velocidade_atual = nova if nova <= maxima else maxima
        return self.velocidade_atual

    # método frear
    def frear(self, delta=5):
        minima = 0
        nova = self.velocidade_atual - delta
        self.velocidade_atual = nova if nova >= minima else minima
        return self.velocidade_atual


if __name__ == '__main__':
    # instância c1 criada a partir de Carro
    c1 = Carro(180)

    for _ in range(25):
        print(c1.acelerar(8))

    for _ in range(10):
        print(c1.frear(delta=20))
