from datetime import datetime

# v2 - adicionando classe Projeto

class Projeto:
    def __init__(self, nome):
        self.nome = nome
        self.tarefas = []

    def add(self, descricao):
        self.tarefas.append(Tarefa(descricao))

    def pendentes(self):
        return [tarefa for tarefa in self.tarefas if not tarefa.feito]

    def procurar(self, descricao):
        # possível index error
        # [0] pega primeiro elemento
        # caso não encontre, pode dar problema quando tentar acessar o [0]
        return [tarefa for tarefa in self.tarefas if tarefa.descricao == descricao][0]

    def __str__(self):
        return f'{self.nome} ({len(self.pendentes())} tarefa(s) pendentes(s))'


class Tarefa:
    def __init__(self, descricao):
        self.descricao = descricao
        self.feito = False
        self.criacao = datetime.now()

    def concluir(self):
        self.feito = True

    # método mágico que retorna string
    def __str__(self):
        return self.descricao + (' (Concluída)' if self.feito else '')


def main():
    casa = Projeto('Tarefas domésticas')
    casa.add('Passar roupa')
    casa.add('Lavar pratos')
    print(casa)

    casa.procurar('Lavar pratos').concluir()
    for tarefa in casa.tarefas:
        print(f'- {tarefa}')
    print(casa)

    mercado = Projeto('Compras no mercado')
    mercado.add('Carne')
    mercado.add('Chocolate')
    print(mercado)


if __name__ == "__main__":
    main()
