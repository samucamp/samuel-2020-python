from datetime import datetime

# v1 - PROJETO TODO LIST


class Tarefa:
    def __init__(self, descricao):
        self.descricao = descricao
        self.feito = False
        self.criacao = datetime.now()

    def concluir(self):
        self.feito = True

    # método mágico que retorna string
    def __str__(self):
        return self.descricao + (' (Concluída)' if self.feito else '')


def main():
    # objetos criados são armazenados na lista casa
    casa = []
    casa.append(Tarefa('Passar roupa'))
    casa.append(Tarefa('Lavar roupa'))

    # uma forma de percorrer a lista casa e concluir determinada tarefa
    '''
    for tarefa in casa:
        if tarefa.descricao == 'Lavar roupa':
            tarefa.concluir()
        print(f'- {tarefa}')
    '''
    # outra forma mais otimizada: list comprehension
    [tarefa.concluir() for tarefa in casa if tarefa.descricao == 'Lavar roupa']
    for tarefa in casa:
        print(f'- {tarefa}')


if __name__ == "__main__":
    main()
