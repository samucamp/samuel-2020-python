from datetime import datetime, timedelta

# v5 - Princípio de Herança


class Projeto:
    def __init__(self, nome):
        self.nome = nome
        self.tarefas = []

    # metodo magico de iteração
    # não precisa acessar mais [projeto].tarefas
    # exemplo for tarefa in mercado.tarefas
    # basta acessar com [projeto]
    # exemplo for tarefa in mercado
    def __iter__(self):
        return self.tarefas.__iter__()

    def add(self, descricao, vencimento=None):
        self.tarefas.append(Tarefa(descricao, vencimento))

    def pendentes(self):
        return [tarefa for tarefa in self.tarefas if not tarefa.feito]

    def procurar(self, descricao):
        # possível index error
        # [0] pega primeiro elemento
        # caso não encontre, pode dar problema quando tentar acessar o [0]
        return [tarefa for tarefa in self.tarefas if tarefa.descricao == descricao][0]

    def __str__(self):
        return f'{self.nome} ({len(self.pendentes())} tarefa(s) pendentes(s))'


class Tarefa:
    def __init__(self, descricao, vencimento):
        self.descricao = descricao
        self.feito = False
        self.criacao = datetime.now()
        self.vencimento = vencimento

    def concluir(self):
        self.feito = True

    # método mágico que retorna string
    def __str__(self):
        status = []
        if self.feito:
            status.append('(Concluída)')
        elif self.vencimento:
            if datetime.now() > self.vencimento:
                status.append('(Vencida)')
            else:
                dias = (self.vencimento - datetime.now()).days
                status.append(f'(Vence em {dias} dias)')
        return f'{self.descricao} '+''.join(status)


# TarefaRecorrente herda de Tarefa (classe pai)
class TarefaRecorrente(Tarefa):
    def __init__(self, descricao, vencimento, dias=7):
        # chamando construtor da classe pai
        # referenciando a classe pai com super()
        super().__init__(descricao, vencimento)
        self.dias = dias

    def concluir(self):
        # reutilizando codigo da classe pai
        super().concluir()
        novo_vencimento = datetime.now() + timedelta(days=self.dias)
        return TarefaRecorrente(self.descricao, novo_vencimento, self.dias)


def main():
    casa = Projeto('Tarefas domésticas')
    casa.add('Passar roupa', datetime.now())
    casa.add('Lavar pratos', datetime.now() + timedelta(days=3, minutes=12))
    casa.tarefas.append(TarefaRecorrente('Trocar lençóis', datetime.now(), 7))
    casa.tarefas.append(casa.procurar('Trocar lençóis').concluir())
    print(casa)

    # sem __iter__
    casa.procurar('Lavar pratos').concluir()
    for tarefa in casa.tarefas:
        print(f'- {tarefa}')
    print(casa)

    mercado = Projeto('Compras no mercado')
    mercado.add('Carne')
    mercado.add('Chocolate')
    mercado.add('Tomate', datetime.now()+timedelta(days=3, minutes=12))
    print(mercado)

    # com __iter__
    for tarefa in mercado:
        print(f'- {tarefa}')


if __name__ == "__main__":
    main()
