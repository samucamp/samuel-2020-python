class MyClass:

    def __init__(self):
        print('constructor')

    def __str__(self):
        return "when you print this class"

    def __add__(self, aux):
        return 'When you add me to ' + aux

    def __eq__(self, aux):
        return 'When you compare me to ' + aux


def main():
    teste = MyClass()
    print(teste)
    print(teste + 'adicional')
    print(teste == 'comparacao')


if __name__ == "__main__":
    main()
