# convenção para tratar variável com constante
MAIOR_IDADE = 18

class Pessoa:
    def __init__(self, nome, idade):
        self.nome = nome
        self.idade = idade

    def is_adulto(self):
        # 0 será utilizando se self.idade for None
        return ((self.idade or 0) > MAIOR_IDADE)

    def __str__(self):
        if not self.idade:
            return self.nome
        return f'{self.nome} ({self.idade} anos)'

    # função criada para teste. __str__ substitui o uso desta.
    # com __str__ basta utilizar print(instância)
    # sem __str__ seria print(instância.método)
    def print_name(self):
        if not self.idade:
            print(self.nome)
        else:
            print(f'{self.nome} ({self.idade} anos)')
