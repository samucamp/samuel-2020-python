# __init__ utilizado para pacote

from .cliente import Cliente
from .vendedor import Vendedor
from .compra import Compra

# expoe classes para fora do pacote
# chama esse pacote com from loja import class1, class2, class3 ...
__all__ = ['Cliente', 'Vendedor', 'Compra']
