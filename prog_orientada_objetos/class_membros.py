# self é o objeto no qual chamou o método
# é o objeto que está em execução no momento


class Data1:
    def to_str(self):
        return f'{self.dia}/{self.mes}/{self.ano}'


class Data2:
    def __str__(self):
        return f'{self.dia}/{self.mes}/{self.ano}'


# cria objeto d1, apartir da instância da classe Data()
d1 = Data1()
d1.dia = 5
d1.mes = 12
d1.ano = 2019
print(d1.to_str())

d1 = Data2()
d1.dia = 5
d1.mes = 12
d1.ano = 2019
print(d1)

d2 = Data1()
d2.dia = 7
d2.mes = 11
d2.ano = 2020
print(d2.to_str())

d2 = Data2()
d2.dia = 7
d2.mes = 11
d2.ano = 2020
print(d2)
