# calculadora.py
class Calculadora:
    def __init__(self, operando1):
        self.num = operando1

    def soma(self, operando2):
        return self.num + operando2

    def subtrai(self, operando2):
        return self.num - operando2

    def multiplica(self, operando2):
        return self.num * operando2

    def divide(self, operando2):
        return self.num / operando2

    def eleva(self, operando2):
        return self.num ** operando2


if __name__ == "__main__":
    numero = Calculadora(5)
    print(numero.soma(3))
    print(numero.multiplica(5))
    print(numero.eleva(3))
