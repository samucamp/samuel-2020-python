# APRENDIZADO
# TIPOS DE MÉTODOS: STATIC, CLASS, INSTÂNCIA (recebe self)
#


class Humano:
    especie = 'Homo Sapiens'

    def __init__(self, nome):
        self.nome = NotImplemented

    # Métodos de instância (recebe self)
    def das_cavernas(self):
        self.especie = 'Homo Neanderthalensis'
        return self

    # Método de estático não recebe nenhum parâmetro
    @staticmethod
    def especies():
        adjetivos = ('Habilis', 'Erectus', 'Neanderthalensis', 'Sapiens')
        return ('Australopiteco',) + tuple(f'Homo {adj}' for adj in adjetivos)

    # Método de classe como convenção utiliza cls (class é reservado da linguagem, não pode usar)
    @classmethod
    def is_evoluido(cls):
        return cls.especie == cls.especies()[-1]

class Neanderthal(Humano):
    especie = Humano.especies()[-2]

class HomoSapiens(Humano):
    especie = Humano.especies()[-1]

def main():
    jose = HomoSapiens('José')
    # HomoSapiens.das_cavernas(jose)

    grokn = Neanderthal('Grokn')

    # Pode acessar metodo classe e estático sem restrição
    print(
        f'Evolução (a partir da classe): {", ".join(HomoSapiens.especies())}')
    print(f'Evolução (a partir da instancia): {", ".join(jose.especies())}')

    # conceito principio da oo polimorfismo 
    # cls pode ser chamado de multiplas formas
    # o mesmo método de classe pode comportar diferente
    print(f'Homo Sapiens evoluído? {HomoSapiens.is_evoluido()}')
    print(f'Neanderthal evoluído? {Neanderthal.is_evoluido()}')
    print(f'José evoluído? {jose.is_evoluido()}')
    print(f'Grokn evoluído? {grokn.is_evoluido()}')

if __name__ == "__main__":
    main()
