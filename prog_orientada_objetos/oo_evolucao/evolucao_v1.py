# APRENDIZADO
# ENCADEAMENTO DE MÉTODOS COM (RETURN SELF)
# DIFERENÇA ENTRE ATRIBUTOS DE CLASSE E DE INSTÂNCIA


class Humano:
    # atributo de classe (estático) compartilhado com todas instâncias
    especie = 'Homo Sapiens'

    # construtor, atributos pertencem a instância=objeto
    def __init__(self, nome):
        self.nome = NotImplemented

    def das_cavernas(self):
        self.especie = 'Homo Neanderthalensis'
        return self


def main():
    jose = Humano('José')

    # SEM ENCADEAMENTO
    #grokn = Humano('Grokn')
    # grokn.das_cavernas()

    # COM ENCADEAMENTO
    # basta retornar self dentro do método
    grokn = Humano('Grokn').das_cavernas()

    print(f'Humano.especie: {Humano.especie}')
    print(f'jose.especie: {jose.especie}')
    print(f'grokn.especie: {grokn.especie}')


if __name__ == "__main__":
    main()
