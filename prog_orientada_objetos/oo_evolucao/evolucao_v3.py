# APRENDIZADO
# Propriedades: get, set (improviso)


class Humano:
    # atributo de classe
    especie = 'Homo Sapiens'

    def __init__(self, nome):
        self.nome = nome
        # conveção _idade atributo privado
        self._idade = None

    # ANTES DE LER O VALOR -> GET
    def get_idade(self):
        return self._idade

    # ANTES DE ALTERAR O VALOR -> SET
    def set_idade(self, idade):
        if idade < 0:
            # validação
            # lança erro com mensagem
            raise ValueError('Idade deve ser um número positivo!')
        self._idade = idade

    def das_cavernas(self):
        self.especie = 'Homo Neanderthalensis'
        return self

    @staticmethod
    def especies():
        adjetivos = ('Habilis', 'Erectus', 'Neanderthalensis', 'Sapiens')
        return ('Australopiteco',) + tuple(f'Homo {adj}' for adj in adjetivos)

    @classmethod
    def is_evoluido(cls):
        return cls.especie == cls.especies()[-1]


class Neanderthal(Humano):
    especie = Humano.especies()[-2]


class HomoSapiens(Humano):
    especie = Humano.especies()[-1]


def main():
    jose = HomoSapiens('José')
    jose.set_idade(40)
    print(f'Nome: {jose.nome} Idade: {jose.get_idade()}')


if __name__ == '__main__':
    main()
