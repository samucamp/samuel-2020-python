# consome menos memória que o list comprehension
# ( expressão for item in list if condition)
# gerador sob demanda

generator = (i ** 2 for i in range(10) if i % 2 == 0)
print(generator)
print(next(generator))  # Saída 0
print(next(generator))  # Saída 4
print(next(generator))  # Saída 16
print(next(generator))  # Saída 36
print(next(generator))  # Saída 64
# print(next(generator))  # Erro!

generator = (i ** 2 for i in range(10) if i % 2 == 0)

for numero in generator:
    print(numero)
