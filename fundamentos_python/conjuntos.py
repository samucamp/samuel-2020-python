# set
# não é indexado
# não aceita repetição
# não garante ordenação
# parecido com lista, com as exceções acima

a = {1, 2, 3}
print(type(a))

a = set('coder')
print(a)
a = set('codddder')
# ignora repetidos
# perde ordenação

print(a)
print('3' in a, 4 not in a)

print({1, 2, 3} == {1, 3, 2, 1})

# operações
c1 = {1, 2}
c2 = {2, 3}
print(c1.union(c2))  # não altera individualmente
print(c1.intersection(c2))

c1.update(c2)  # altera o conjunto 1
print(c1, c2)

# checar se é subconjunto ou superconjunto
print(c2 <= c1)
print(c1 >= c2)

print({1, 2, 3} - {2})
print(c1-c2)

c1 -= {2}
print(c1)
