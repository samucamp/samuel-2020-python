pessoa = {'nome': 'prof(a) Ana', 'idade': 38, 'cursos': [
    'Inglês', 'Português']}

# index por string é mais comum, 'idade':

print(type(pessoa))

# print(dir(dict))
# lembra objetos, mas é diferente !

print(pessoa)
print(len(pessoa))

print(pessoa['nome'])
print(pessoa['cursos'])
print(pessoa['cursos'][1])
print(pessoa.keys())
print(pessoa.values())
print(pessoa.items())
print(pessoa.get('idade'))
print(pessoa.get('tags'))  # vazio

pessoa['idade'] = 55
print(pessoa['idade'])

pessoa['cursos'].append('Angular')
print(pessoa)

pessoa.pop('idade')  # mostra e remove
print(pessoa)

del pessoa['cursos']
print(pessoa)

pessoa.clear()
print(pessoa)
