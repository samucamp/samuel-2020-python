# print(dir(str))

from string import Template

nome = 'Saulo Pedro'
print(nome)
print(nome[0])
print(nome[6])
print(nome[-1])
print(nome[-2])
print(nome[4:])
print(nome[-2:])
print(nome[:3])
print(nome[2:5])
# nome[0] = 'P' a string é imutável
# é possível mudar o conteúdo da variável]
nome = 'Assim pode'
print(nome)

# print('marca d'agua') não é possível
# alternativas
print("Dias D'Avila")
print('Dias D\'Avila')
print("Dias D'Avila" == 'Dias D\'Avila')

doc = """texto com multiplas
... linhas """
print(doc)
print('texto com multiplas\n... linhas')
doc2 = ''' assim tambem
... pode'''
print(doc2)

numeros = '1234567890'
print(numeros)
print(numeros[::])
print(numeros[::2])
print(numeros[1::2])
print(numeros[::-1])  # inversão
print(numeros[::-2])

frase = 'Python eh uma liguagem excelente'
print('py' in frase)
print('Py' in frase)
print('ing' in frase)
print('ing' not in frase)
print(frase.lower())
print(frase.upper())
print(frase)
# precisa atribuir pra mudar de fato
frase = frase.upper()
print(frase)
print(frase.split())
print(frase.split('E'))  # SPLIT PELA LETRA E

# print(dir(str))
# help(str.center)

a = '123'
b = ' de Oliveira 4'
print(a+b)
print(a.__add__(b))
print(str.__add__(a, b))

print(len(a))
print(a.__len__())

print('1' in a)
print(a.__contains__('1'))


# INTERPOLAÇÃO DE STRING

nome, idade = 'Ana', 30
flutuante = 3.444
print('nome: %s Idade: %d' % (nome, idade))  # mais antigo
print('flutuante: %.2f' % flutuante)
print('teste %r %r' % (True, False))

print('Nome: {0} Idade: {1}'.format(nome, idade))  # python < 3.6

print(f'Nome: {nome} Idade: {idade} {2 ** 8 +1}')  # mais recente

# from string import Template

s = Template('Nome: $n Idade: $i')
print(s.substitute(n=nome, i=idade))
