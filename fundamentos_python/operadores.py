# -*- coding: utf-8 -*-
# Operadores Binarios

# 2 operandos e 1 operador no meio, operador binário
# nomenclatura infix, pois operador fica no meio de 2 op.
print(2 + 3)
print(4 - 7)
print(2 * 5.3)
print(9.4 / 3)
print(9.4 // 3)
# para retornar inteiro usa //
# como usou float 9.4, retorna inteiro float 3.0

print(2 ** 8)
# exponenciação, 2 elevado a 8

print(10 % 3)
# modulo, ou resto da divisão

a = 12
b = a
print(a + b)

# Cálculo de percentual
salario = 3450.45
despesas = 2456.2
percentual_comprometido = despesas * 100 / salario
print(percentual_comprometido)

# Operadores relacionais
print(3 > 4)
print(4 >= 3)
print(1 < 2)
print(3 <= 1)
print(3 != 2)
print(3 == 3)
print(2 == '2')
# em python é falso
# em JS é true
# em JS só é falso para ===

# Operadores de atribuições
a = 3
a = a + 7
print(a)

# a = 5
a += 5  # a = a + 5
print(a)

a -= 3
print(a)

a *= 2
print(a)

a /= 4
print(a)

a %= 4
print(a)

a **= 8
print(a)

a //= 256
print(a)


# Operadores Lógicos

# Lógica E
print(False and False)
print(False and True)
print(True and True)

# Lógica OU
print(False or False)
print(False or True)
print(True or True)

# Lógica XOR
print(False != False)
print(False != True)
print(True != False)
print(True != True)

# Negação (Unário)
print(not True)
print(not False)
print(not not 0)  # checar se é verdadeiro ou falso
print(not not 1)  # checar se é verdadeiro ou falso

# Operadores Bit a Bit (CUIDADO !)
print(True & True)
print(True | True)
print(True ^ False)

# 3 = 11
# 2 = 10
# 11 and 10 bit a bit
# resultado = 10
# não gera true or false, gera resultado da operação bit a bit
# 3 & 2


# OPERADORES UNÁRIOS

a = 3
# a++  não existe em python
a += 1  # equivale a a++
# a-- não existe em python
a -= 1  # equivale a a--

-a  # transforma em negativo
+a  # mantem


# OPERADORES TERNÁRIOS
esta_chuvendo = False
# primeira opção é pra falso
# segunda opção é true
print('Hoje estou com as roupas ' + ('secas.', 'molhadas.')[esta_chuvendo])
print('Hoje estou com as roupas ' +
      ('molhadas.' if esta_chuvendo else 'secas.'))


# OPERADORES DE MEMBRO
lista = [1, 2, 3, 'Ana', 'Carla']
print(2 in lista)
print('Ana' not in lista)

# OPERADORES DE IDENTIDADE
x = 3
y = x
z = 3
print(x is y)
print(y is z)
print(x is not z)
# nesse caso, a variavel armazena cópia do valor


lista_a = [1, 2, 3]
lista_b = lista_a
lista_c = [1, 2, 3]
print(lista_a is lista_b)
print(lista_b is lista_c)
print(lista_a is not lista_c)
lista_a[1] = 400  # altera valor dentro do espaço de memoria que lista_a aponta
# verificar alteração utilizando python preview
# nesse caso, lista_b aponta para o espaço de memoria que a lista_a aponta
# lista_c aponta para outro espaço de memoria com os mesmos valores
