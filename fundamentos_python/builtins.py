# type()
# funções presentes dentro do builtins
# import math pra mostrar que aparece no dir()
print(type(1))
print(__builtins__.type('Fala galera!'))
__builtins__.print(10/3)

# __builtins__.help(__builtins__.dir)
# retorna list of objects

# 1+2
# _ * 3
# resulta em 9
# _ pega ultima operação
# dir() printa tudo disponivel no escopo global


print(dir())
print(dir(__builtins__))

nome = 'Joao da Silva'
print(type(nome))
print(__builtins__.len(nome))  # retorna length da string
print(dir())  # inclui nome no escopo global
