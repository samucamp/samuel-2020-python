# tupla não consegue ser modificada

tupla = tuple()
tupla = ()
print(type(tupla))

# print(dir(tupla))

# help(tuple)

tupla = ('um')
print(type(tupla))
tupla = ('um',)
print(type(tupla))
print(tupla[0])
# tupla[0] = 'novo' da erro, imutável

cores = ('verde', 'amarelo', 'azul', 'branco')

# comandos iguais aos usados na lista, index, count, len
# tupla não pode ser modificada
# sintaxe diferente ('','', ...)
# se for tupla de unico elemento, precisa de virgula, senão vira String
