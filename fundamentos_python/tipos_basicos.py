print('Primeiro programa')
# não precisa definir tipos
# cuidado com identação no python
# para ignorar quebra de linha, utiliza \
a = \
    5
b = 4


# em python é permitido trocar o valor da variavel dessa forma
b, a = 5, 4
3+2
print(a, b)

print(True)
print(False)  # F e T maiusculo

print(1.2 + 1)  # real + inteiro = real

print('Aqui eh uma string!')
print("Pode ser assim tambem")

# concatena strings, multiplicação repete string
print('Voce eh ' + 3 * 'muito ' + 'legal!')

# não pode ambiguidade ex:
# print('1' + 1)
# por isso é fortemente tipada
# pois valida se pode ser misturado dois tipos

# array é chamado de lista no python
lista = [1, 2, 3]
print(lista)

# objeto é um dicionário com chave e valor
dicionario = {'nome': 'Pedro', 'idade': 27}
print(dicionario)

# python é dinamicamente tipada (permite mudar tipo variavel)
a = "Agora sou uma string"
print(a)

"COMENTÁRIOS PODE FAZER ASSIM"
'COMENTÁRIO PODE FAZER ASSIM TAMBÉM'

# string format
# lembra template string de JS
teste = 32
print(" Teste = {}".format(teste))
teste2 = 324
teste4 = 234
print("string1 = {1}, string2 = {0}, string3 = {2}".format(
    teste, teste2, teste4))
# associa par de chaves com um valor do .format
# pode mudar a ordem dos indices

# CONVERSÃO DE TIPOS

a = 2
b = '3'

print(type(a))
print(type(b))

print(a + int(b))
print(str(a) + b)

print(type(str(a)))
print(type(int(b)))

# int('2 legal') não funciona, não converte
# int('2.4') não converte também
print(float('2.4'))  # converte pra 2.4 float

# COERÇÃO AUTOMÁTICA

print(10/2)  # resulta em valor exato, mas float 5.0
print(type(10/2))

print(10/3)
print(type(10/3))

print(10//3)  # forçando valor inteiro
print(type(10//3))

print(10//3.3)  # tentativa de forçar valor inteiro
# operação inteiro float = float
# float tem mais capacidade de armazenamento
print(type(10//3.3))

print(2+True)  # não há ambiguidade, True = 1
print(1+False)  # não há ambiguidade, False = 1
