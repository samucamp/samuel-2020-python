# python, lista (class) é heterogênea e dinâmica.

lista = []
print(type(lista))
# print(dir(lista))
# help(list)

print(len(lista))

lista.append(1)
lista.append(5)
print(lista)

print(len(lista))

# evitar trabalhar com lista heterogênea
nova_lista = [1, 5, 'Ana', 'Bia']
print(nova_lista)
nova_lista.remove(5)
print(nova_lista)
nova_lista.reverse()
# string é imutavel e precisaria atribuir novamente
# no caso da lista, a função já altera sem atribuir (mutável)
print(nova_lista)

lista = [1, 5, 'Rebeca', 'Guilherme', 3.1415]
print(lista.index('Guilherme'))
print(lista.index(1))
# print(lista.index(42))
print('Rebeca' in lista)
print(lista[2])
print(lista[0])
print(lista[-1])
print(lista[-5])
# print(lista[5])

lista = ['Ana', 'Lia', 'Rui', 'Paulo', 'Dani']
print(lista[1:3])
print(lista[1:-1])
print(lista[1:])
print(lista[:-1])
print(lista[:])
print(lista[::2])
print(lista[::-1])
del lista[2]
print(lista)
del lista[1:]
print(lista)
