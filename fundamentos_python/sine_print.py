import math
import time


numCycle = 5
pi = math.pi
x = 0


def sin(x):
    return math.sin(x)


# converting to radianos and printing
while x < (numCycle*2*pi):
    # int to produce integer number
    bar = int(20*sin(x))
    # sin returns -1 to 1
    # x = x+0.3
    x += 0.3
    # shift sine wave for only positive
    print((21+bar) * '=')
    # delay
    time.sleep(0.03)
