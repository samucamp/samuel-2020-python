# -*- coding: utf-8 -*-
# TIPOS BÁSICOS
# DETALHANDO EM TIPOS NUMÉRICOS
import decimal
from decimal import Decimal, getcontext

print(dir(int))
print(dir(float))

b = 2.5
print(b.is_integer())
print(5.0.is_integer())

print(int.__add__(2, 3))

print((-2).__abs__())

print((-3.6).__abs__())

print(abs(-3))

print(1.1 + 2.2)  # ocorre aproximação
# utiliza mesma especificação que JS para aproximação

# para decimal (Não está no builtin), precisa fazer import

print(Decimal(1)/Decimal(7))

getcontext().prec = 4

print(Decimal(1)/Decimal(7))
print(Decimal.max(Decimal(1), Decimal(7)))

print(1.1 + 2.2)
getcontext().prec = 10
print(Decimal(1.1)+Decimal(2.2))

print(dir(decimal))
