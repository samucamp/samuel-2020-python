
arquivo = open('pessoas.csv')
dados = arquivo.read()  # captura os dados e armazena em dados
# nesse caso, o arquivo é carregado completo
arquivo.close()

for registro in dados.splitlines():
    # print(registro.split(','))
    # print(*registro.split(',')) remove strings da tupla
    print('Nome: {}, Idade: {}'.format(*registro.split(',')))


arquivo = open('pessoas.csv')
# nesse caso, o arquivo é carregado por partes
# leitura stream
for registro in arquivo:
    print('Nome: {} Idade: {}'.format(*registro.split(',')))
arquivo.close()

# removendo os espaços
arquivo = open('pessoas.csv')
for registro in arquivo:
    print('Nome: {} Idade: {}'.format(*registro.strip().split(',')))
arquivo.close()
