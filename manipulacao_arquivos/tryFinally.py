# garantindo que o arquivo será fechado

try:
    arquivo = open('pessoas.csv')

    for registro in arquivo:
        print('Nome: {} Idade: {}'.format(registro.strip().split(',')))
finally:
    # finally sempre será executado
    print('Finally.')
    arquivo.close()
    if arquivo.closed:
        print('Arquivo fechado!')

# não executa se tiver erro
print('Leitura realizada')
