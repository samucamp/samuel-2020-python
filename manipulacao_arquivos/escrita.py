with open('pessoas.csv') as arquivo:
    # w define write
    with open('pessoas.txt', 'w') as saida:
        for registro in arquivo:
            pessoa = registro.strip().split(',')
            print('Nome: {} Idade: {}'.format(*pessoa), file=saida)

if arquivo.closed:
    print('bloco WITH garante que arquivo será fechado')

if saida.closed:
    print('bloco WITH também garante arquivo de saída')
