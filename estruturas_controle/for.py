from random import randint

###################################################
# AULA FOR 1
for i in range(1, 11):
    print('i = {}'.format(i))

for j in range(10):
    print(f'j = {j}')

for x in range(1, 11):
    for y in range(1, 11):
        print(f'{x} * {y} = {x * y}')

###################################################
# AULA FOR 2

palavra = 'paralelepípedo'
for letra in palavra:
    print(letra, end=',')
    # para imprimir na mesma linha, altera end
print('Fim')

aprovados = ['Rafaela', 'Pedro', 'Renato', 'Maria']
for nome in aprovados:
    print(nome)

for posicao, nome in enumerate(aprovados):
    print(f'{posicao + 1})', nome)

dias_semana = ('Domingo', 'Segunda', 'Terça',
               'Quarta', 'Quinta', 'Sexta', 'Sábado')
for dia in dias_semana:
    print(f'Hoje é {dia}')

# set não garante ordem
for letra in set('muito legal'):
    print(letra, end=', ')

# set literal
for numero in {1, 2, 3, 4, 5, 6}:
    print(numero, end=', ')

###################################################
# AULA FOR 3
# DICIONÁRIO
produto = {'nome': 'Caneta Chic', 'preco': 14.99,
           'importada': True, 'estoque': 793}

# por padrão percorre produto.keys
for chave in produto:
    print(chave)

# para percorrer valor, deve definir .values()
for valor in produto.values():
    print(valor)

for chave, valor in produto.items():
    print(chave, '=', valor)

# após declarar no for, estão disponíveis fora do laço
print(chave, valor)

###################################################
# AULA FOR 4


# from random import randint

# FOR e ELSE
# else será executado no final
for i in range(1, 11):
    print(i)
else:
    print('Fim!')

# nesse caso da break antes de executar o else
for i in range(1, 11):
    if i == 6:
        break
    print(i)
else:
    print('Fim!')

# dado 6 numeros entre 1 e 6
# for com range de 1 a 6
# se for impar continue
# se for par e for igual ao sorteado
# pela funcao dado imprimir 'ACERTOU' e depois chamar o break
# se nao acertar chama o else... print(' não acertou..')


def sortear_dado():
    return randint(1, 6)


# no range o segundo parâmetro é incluso
for i in range(1, 7):
    if i % 2 == 1:
        continue

    if sortear_dado() == i:
        print('ACERTOU', i)
        break
else:
    print('Não acertou o número!')

    # FOR SEM ELSE
PALAVRAS_PROIBIDAS = ('futebol', 'religião', 'política')
textos = [
    'João gosta de futebol e política',
    'A praia foi divertida',
]

for texto in textos:
    found = False
    for palavra in texto.lower().split():
        if palavra in PALAVRAS_PROIBIDAS:
            print('Texto possui pelo menos uma palavra proibida:', palavra)
            found = True
            break

    if not found:
        print('Texto autorizado:', texto)

    # FOR COM ELSE TROCA 'if not found' por else
    # não precisa da variável found

    # FOR COM SET

    PALAVRAS_PROIBIDAS = {'futebol', 'religião', 'política'}
textos = [
    'João gosta de futebol e política',
    'A praia foi divertida',
]

for texto in textos:
    intersecao = PALAVRAS_PROIBIDAS.intersection(set(texto.lower().split()))
    if intersecao:
        print('Texto possui palavras proibidas:', intersecao)
    else:
        print('Texto autorizado:', texto)
