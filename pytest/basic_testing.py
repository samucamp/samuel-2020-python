# test sample from pytest org

# para testar:
# $ pytest basic_testing.py


def func(x):
    return x+1


def test_answer():
    assert func(1) == 2
    assert func(3) == 5
    # o programa para ao achar o primeiro erro
    assert func(5) == 6
    assert func(2) == 2
