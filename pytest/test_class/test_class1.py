class TestClass01:

    def test_case01(self):
        assert 'python'.upper() == 'PYTHON'

    def test_Case02(self):
        assert 'PYTHON'.lower() == 'python'

# para testar individual
# pytest test_class1.py
# ou
# pytest test_class1.py -v

# para testar o pacote test_class
# pytest test_class/ -v