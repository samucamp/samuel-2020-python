class TestClass02:

    def func(self, x):
        return x+1

    def test_answer(self):
        assert self.func(1) == 2
        assert self.func(3) == 5
        # o programa para ao achar o primeiro erro
        assert self.func(5) == 6
        assert self.func(2) == 2

# para testar individual
# pytest test_class2.py
# ou
# pytest test_class2.py -v

# para testar o pacote test_class
# pytest test_class/ -v
