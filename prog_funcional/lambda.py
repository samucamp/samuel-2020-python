# Lambda function
# Função anônima

# Utilizado map(function, list)
# map retorna uma nova lista e não altera a original

compras = (
    {'quantidade': 2, 'preco': 10},
    {'quantidade': 3, 'preco': 20},
    {'quantidade': 5, 'preco': 14},
)

totais = tuple(
    map(
        lambda compra: compra['quantidade'] * compra['preco'],
        compras
    )
)

# ALTERNATIVA A LAMBDA
# def calc_preco_total(compra):
#     return compra['quantidade'] * compra['preco']
#
#
# totais = tuple(map(calc_preco_total, compras))

print('Preços totais:', totais)
print('Total geral:', sum(totais))
