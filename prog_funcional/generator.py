# Iterables
# When you create a list, you can read its items one by one. Reading its items one by one is called iteration
# These iterables are handy because you can read them as much as you wish,
# but you store all the values in memory and this is not always what you want when you have a lot of values.

# Generators
# Generators are iterators, a kind of iterable you can only iterate over once.
# Generators do not store all the values in memory, they generate the values on the fly.

# Yield
# yield is a keyword that is used like return, except the function will return a generator.
# When you call the function, the code you have written in the function body does not run.
# The function only returns the generator object, this is a bit tricky


def cores_arco_iris():
    yield 'vermelho'
    yield 'laranja'
    yield 'amarelo'
    yield 'verde'
    yield 'azul'
    yield 'índigo'
    yield 'violeta'


def sequence():
    num = 0
    while True:
        num += 1
        yield num


def main():
    generator = cores_arco_iris()
    print(type(generator))

    # while True:
    #     print(next(generator))
    # da erro no codigo pois não verifica quando finaliza o indice
    # for _ in _ já resolve esse problema internamente
    for cor in generator:
        print(cor)
    print('Fim!')

    seq = sequence()
    print(next(seq))
    print(next(seq))
    print(next(seq))
    print(next(seq))
    print(next(seq))
    print(next(seq))
    print(next(seq))
    print(next(seq))
    print(next(seq))


if __name__ == '__main__':
    main()
