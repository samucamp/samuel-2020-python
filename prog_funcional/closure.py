# Função Closure
# Função tem ciência de onde está definida
# ex. dentro de outra função


def multiplicar(x):
    def calcular(y):
        # para função calcular, a variável x da função multiplicar (sua closure) também é definida
        return x * y
    return calcular

# a função calcular pode ser retornada devido a capacidade da linguagem python no paradigma de programação funcional


if __name__ == '__main__':
    dobro = multiplicar(2)
    triplo = multiplicar(3)
    print(dobro)
    print(triplo)
    print(f'O triplo de 3 é {triplo(3)}')
    print(f'O triplo de 3 é {multiplicar(3)(3)}')
    print(f'O dobro de 7 é {dobro(7)}')
    print(f'O dobro de 7 é {multiplicar(2)(7)}')
