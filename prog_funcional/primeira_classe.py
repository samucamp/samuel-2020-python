# CONCEITO FIRST CLASS FUNCTION
# Permite trabalhar com funções como se fossem dados


def dobro(x):
    return x * 2


def quadrado(x):
    return x ** 2


if __name__ == '__main__':
    d = dobro
    print(d(5))

    q = quadrado
    print(q(5))

    # Retornar alternadamente o dobro ou quadrado nos números de 1 a 10
    # funções podem ser armazenadas em uma lista, como se fosse dados
    # lista * 5, repete a lista 5 vezes.
    funcs = [dobro, quadrado] * 5

    # func = função dentro de funcs por index
    # numero = valor dentro de range(1, 11)
    # zip coloca os dados em uma tupla
    for func, numero in zip(funcs, range(1, 11)):
        # print(func)
        # print(numero)
        print(f'O {func.__name__} de {numero} é {func(numero)}')
