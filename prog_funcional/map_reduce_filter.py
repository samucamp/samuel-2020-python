from functools import reduce

lista_1 = [1, 2, 3]

lista_2 = [
    {'nome': 'João', 'idade': 31},
    {'nome': 'Maria', 'idade': 37},
    {'nome': 'José', 'idade': 26}
]

lista_3 = [
    {'nome': 'Pedro', 'idade': 11},
    {'nome': 'Mariana', 'idade': 18},
    {'nome': 'Arthur', 'idade': 26},
    {'nome': 'Rebeca', 'idade': 6},
    {'nome': 'Tiago', 'idade': 19},
    {'nome': 'Gabriela', 'idade': 17},
]


# Dobro MAP
dobro = map(lambda x: x*2, lista_1)
print(dobro)
print(list(dobro))
print(tuple(dobro), "\n")


# Ao quadrado MAP
elevado = map(lambda x: x**2, lista_1)
print(list(elevado), "\n")


# So nomes MAP
so_nomes = map(lambda pessoa: pessoa['nome'], lista_2)
print(so_nomes)
print(list(so_nomes))
print(tuple(so_nomes), "\n")


# So idade MAP
so_idade = map(lambda pessoa: pessoa['idade'], lista_2)
print(list(so_idade))


# nome... tem idade... MAP
frase = map(
    lambda pessoa: f'{pessoa["nome"]} tem {pessoa["idade"]} anos.', lista_2)
print(list(frase), "\n")

# Filter só retorna expressão verdadeira
# pessoas com nome grade FILTER
nome_grande = filter(lambda pessoa: len(pessoa['nome']) > 6, lista_3)
print(nome_grande)
print(list(nome_grande), "\n")


# soma das idades de todos os menores de idade MAP FILTER REDUCE
so_idade = map(lambda pessoa: pessoa['idade'], lista_3)
print(list(so_idade))
menores = filter(lambda idade: idade < 18, so_idade)
soma_idades = reduce(lambda idades, idade: idades + idade, menores, 0)
print(soma_idades)
