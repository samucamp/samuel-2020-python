'''
Obs: Todas as funções devem ser desenvolvidas em Python 3 e devem ser devidamente documentadas (docstring) e testadas. Para testá-las, crie um script de testes com o número que julgar adequado de exemplos de testes por função. O arquivo de solução deve ser subido no AVA até a data estabelecida. 

Crie uma função chamada Period que receba uma lista de números reais com 30 elementos e que verifique se tais números correspondem a uma sequência periódica. Caso seja periódico, que retorne os elementos de um período, bem como a posição de origem do primeiro elemento daquele período. 
'''


# TA BUGADO... EXECUTA E VERIFICA O ERRO
# REMOVE O break EM LINHA 43 PRA VERIFICAR O ERRO


def Period(lista):
    # Para chamar essa DOCSTRING basta utilizar help(Period), ou print(Period.__doc__())
    '''
    Utilizando a função Period:
    explicacao
    '''
    lprova = []

    for i in range(0, 30, 1):
        lprova.append(lista[i])
        if len(lprova) >= 2:
            print(lprova)
            aux = len(lprova)
            periodico = True
            for x in range(0, 30, aux):
                j = 0
                if periodico:
                    while j < aux:
                        if lprova[j] == lista[x]:
                            print('igual')
                            periodico = True
                        else:
                            print('diferente')
                            periodico = False
                            break
                        j = j + 1
                        x = x + 1
                else:
                    break
            if periodico:
                print('é periodico com {} valores'.format(aux))
                break
            else:
                print('nao é periodico')


# repetindo 1,3,7
lista1 = [1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7,
          1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7, 1, 3, 7]
Period(lista1)
help(Period)
