'''
Crie uma função chamada AddArrays que seja obrigatório o recebimento de 2 listas de números reais com 7 e 10 elementos, e que seja optativo o recebimento da posição que determina a origem de cada lista, pois caso não seja informado, a origem será a posição zero da respectiva lista. A função deverá primeiramente igualar o número de elementos das listas a partir das origens adicionando zeros à esquerda e/ou à direita, gerando duas novas listas parciais. Por fim, as listas parciais deverão ser somadas e a função deverá retornar tal lista e sua posição de origem.

Obs: Todas as funções devem ser desenvolvidas em Python 3 e devem ser devidamente documentadas (docstring) e testadas.
Para testá-las, crie um script de testes com o número que julgar adequado de exemplos de testes por função.
'''

# l1 = lista 1
# l2 = lista 2
# o1 = origem da lista 1, valor padrão = 0
# o2 = origem da lista 2, valor padrão = 0


def AddArrays(l1, l2, o1=0, o2=0):
    # Para chamar essa DOCSTRING basta utilizar help(AddArrays), ou print(AddArrays.__doc__())
    '''
    Utilizando a função AddArrays:
        AddArrays(lista 01, lista 02, origem da lista 01, origem da lista 02)
    lista 01 obrigatoriamente deve conter 7 valores reais
    lista 02 obrigatoriamente deve conter 10 valores reais
    A função imprime uma lista 03 que é a soma de lista 01 e lista 02 com origem modificadas, adicionando zero.
    A função também retorna a lista 03 e o número de origem em uma lista [l3, o3]
    '''

    for i in range(o1):
        l1.pop(i)
        l1.insert(i, 0)

    for i in range(o2):
        l2.pop(i)
        l2.insert(i, 0)

    if len(l2) > len(l1):
        for i in range(len(l2)-len(l1)):
            l1.append(0)

    print('lista 01 com zeros: {0}, origem = {1}'.format(l1, o1+1))
    print('lista 02 com zeros: {0}, origem = {1}'.format(l2, o2+1))

    l3 = []
    for i in range(len(l1)):
        l3.append(l1[i] + l2[i])
    print('soma l1+l2 = {0}.\n'.format(l3))

    o3 = o1 if o1 < o2 else o2
    return [l3, o3]


teste1 = AddArrays([13, 22, 0, 4.4, 5, 0, 7], [
    11.2, 23.2, 3, 42, 5, 61, 7, 8, 9, 10], 4, 3)
teste2 = AddArrays([1, 2, 3, 4, 5, 6, 7], [
                   1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 5, 7)

print('Resultado do teste 01: {0}\nOrigem do teste 01: {1}'.format(
    teste1[0], teste1[1]+1))
print('Resultado do teste 02: {0}\nOrigem do teste 02: {1}'.format(
    teste2[0], teste2[1]+1))
print('\n')

help(AddArrays)
