import matplotlib.pyplot as plt
import numpy as np

x = np.arange(10)

# changin color
plt.plot(x, x**2, 'o')
plt.plot(x, x**3, '--')
plt.plot(x, 2*x, ':')
plt.plot(x, 2**x, '>')
plt.show()

# parametros nomeados
plt.plot(x, x, color='g', linestyle='--', linewidth=1.5, marker='o',
         markerfacecolor='b', markeredgecolor='k', markeredgewidth=1.5, markersize=5)
plt.show()