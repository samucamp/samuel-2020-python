import matplotlib.pyplot as plt
import numpy as np

x = np.arange(10)
plt.plot(x, x**2)
plt.plot(x, x**3)
plt.plot(x, x*2)
plt.plot(x, 2**x)
plt.show()

# same result
plt.plot(x, x**2, x, x**3, x, x*2, x, 2**x)
plt.show()

# with array
x = np.array([[1, 2, 6], [4, 5, 3]])
print(x)
# it will plot (1 to 4), (2 to 5), (6 to 3)
plt.plot(x)
plt.show()

# random
data = np.random.randn(2, 10)
print(data)
plt.plot(data[0], data[1])
plt.show()
