import numpy as np
import matplotlib.pyplot as plt

x = np.arange(5)
y = x
print(x, y)

#plt.plot(x, y, 'o')
#plt.plot(x, y, 'o-')
plt.plot(x, y, 'o--')
plt.plot(x, -y, 'o-')
plt.title('y=x and y=-x')

# PARA MOSTRAR O GRÁFICO
plt.show()

# LINSPACE
N = 11
# cria vetor de 0 a 10 com N numeros dentro
x = np.linspace(0, 11, N)
print(x)

# LOGSPACE
y = np.logspace(0.1, 1, N)
plt.plot(x, y, 'o--')
plt.show()

# OPÇÃO REMOVER O AXIS DO GRÁFICO
plt.axis('off')
plt.plot(x, y, 'o')
plt.show()

# GEOMSPACE
y = np.geomspace(0.1, 1000, N)
print(y)
plt.plot(x, y, 'o--')
plt.show()
