import matplotlib.pyplot as plt
import numpy as np

x = np.arange(10)


# individual label para legend()
plt.plot(x, x**2, label="x^2")
plt.plot(x, x**3, label="x^3")
plt.plot(x, 2*x, label="2x")
plt.plot(x, 2**x, label="2^x")
plt.legend(loc='upper center')  # muda localizacao


# plt.plot(x, x**2, x, x**3, x, x*2, x, 2**x)
# plt.legend(['x^2','x^3','2x','2^x'])


# grid
plt.grid(True)


# axis
print(plt.axis())
plt.axis([-1, 12, -100, 1000])
# plt.xlim([-1, 12])
# plt.ylim([-100, 1000])
print(plt.axis())


# label
plt.xlabel('x = np.arange(10)')
plt.ylabel('y = f(x)')


# title
plt.title('Simple plot')


# saving plot
# plt.savefig('test.png')
# will be saved in repository folder


plt.show()
