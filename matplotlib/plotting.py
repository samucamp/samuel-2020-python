import matplotlib.pyplot as plt
import numpy as np

x = [1, 4, 5, 2, 3, 6]
plt.plot(x)
plt.show()


x = np.arange(10)
plt.plot(x)
plt.show()

plt.plot(x, x**2)
plt.show()

plt.plot(x, [y**2 for y in x])
plt.show()
