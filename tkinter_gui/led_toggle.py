# FROM UDEMY COURSE
# Raspberry Pi Workshop 2018 Become a Coder / Maker / Inventor


from tkinter import *
import tkinter.font


# GUI DEFINITION #
win = Tk()
win.title('LED Toggle GUI')
myFont = tkinter.font.Font(family='Helvetica', size=12, weight='bold')


# EVENTS #
def buttonPress():
    print('Button was pressed')


'''
Utilizando hardware com led
bibliotecas RPi.GPIO e gpiozero LED
def ledToggle():
    if led.is_lit:
        led.off()
        toggleButton["text"] = "Turn on LED"
    else:
        led.on()
        toggleButton["text"] = "Turn LED off"
'''


def close():
    win.destroy()  # closes GUI


# WIDGETS #
toggleButton = Button(master=win, text='Toggle', font=myFont,
                      command=buttonPress, bg='green', height=3, width=20)
toggleButton.grid(row=0, column=1)

exitButton = Button(master=win, text='Exit', font=myFont,
                    command=close, bg='red', height=2, width=10)
exitButton.grid(row=1, column=1)

# exit closing app
win.protocol("WM_DELETE_WINDOW", close)

# LOOP #
win.mainloop()
