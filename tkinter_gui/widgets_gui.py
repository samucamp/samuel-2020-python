# FROM UDEMY COURSE
# Raspberry Pi Workshop 2018 Become a Coder / Maker / Inventor


from tkinter import *
import tkinter.font


# GUI DEFINITIONS #
win = Tk()
win.title("Useful Widgets GUI")
myFont = tkinter.font.Font(family='Helvetica', size=12, weight='bold')


#back = Frame(master=win, width=500, height=500, bg='black')
# back.pack()
leftFrame = tkinter.Frame(win)
leftFrame.pack(side=LEFT)
rightFrame = tkinter.Frame(win)
rightFrame.pack(side=RIGHT)
midFrame = tkinter.Frame(win)
midFrame.pack(side=RIGHT)


# VARS #
checkVal1 = IntVar()
checkVal2 = IntVar()
sliderval = DoubleVar()
rad = IntVar()
option = IntVar()


# EVENTS #
def buttonPress():
    print('Slider value is {}'.format(sliderval.get()))


def checkToggle():
    print('\nCheck Box #1 {0} \nCheck Box #2 {1}'.format(
        checkVal1.get(), checkVal2.get()))


def checkRadio():
    select = "Radio button " + str(rad.get()) + " selected"
    label.config(text=select)


def changeOption():
    print('The chosen option is {}'.format(option.get()))


def close():
    win.destroy()  # closes GUI


# WIDGETS #
# button triggers command when pressed
myButton = Button(midFrame, text='Load', font=myFont,
                  command=buttonPress, bg='bisque2', height=1)
myButton.pack(side=BOTTOM)


# checkbox toggles variable and triggers command
myCheckbox1 = Checkbutton(leftFrame, text='Lights  ',
                          variable=checkVal1, command=checkToggle)
myCheckbox1.pack()
myCheckbox2 = Checkbutton(leftFrame, text='Sprinkler',
                          variable=checkVal2, command=checkToggle)
myCheckbox2.pack()


# radio buttons and label
label = Label(leftFrame)
label.pack()
label.config(text="No option selected")
R1 = Radiobutton(leftFrame, text='Option1', variable=rad,
                 value=1, command=checkRadio)
R1.pack()
R2 = Radiobutton(leftFrame, text='Option2', variable=rad,
                 value=2, command=checkRadio)
R2.pack()


# slider bar continuos adjust of variable
mySlider = Scale(midFrame, variable=sliderval, from_=100.0, to=0.0)
mySlider.pack()


# spinbox
numOption = Spinbox(rightFrame, from_=1, to=11, width=5,
                    textvariable=option, command=changeOption)
numOption.pack(side=TOP)


# exit
exitButton = Button(rightFrame, text='Exit', font=myFont,
                    command=close, bg='red', height=1, width=6)
exitButton.pack(side=BOTTOM)


# exit closing app
win.protocol("WM_DELETE_WINDOW", close)


# LOOP #
win.mainloop()
