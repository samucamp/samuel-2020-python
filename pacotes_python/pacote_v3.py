from pacote1 import modulo1
from pacote2 import modulo1 as modulo1_sub
# resolve conflito de modulo1, usando alias (Apelido)


print('Soma', modulo1.soma(3, 2))
print('Sub', modulo1_sub.subtracao(3, 2))
