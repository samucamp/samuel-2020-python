from calc import soma, subtracao

print('Soma', soma(3, 2))
print('Subtracao', subtracao(3, 2))

# Docstring para info sobre a função
print(soma.__doc__)  # imprime docstring
help(soma)  # gera ajuda sobre a função utilizando o docstring

print(subtracao.__doc__)
help(subtracao)
