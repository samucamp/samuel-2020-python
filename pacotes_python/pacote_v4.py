# importand funções diretamente do modulo

from pacote1.modulo1 import soma
from pacote2.modulo1 import subtracao
from pacote2.modulo1 import subtracao as sub
# tambem pode colocar apelido 'as sub'

print('Soma', soma(3, 2))
print('Subtracao', subtracao(3, 2))
print('Sub', sub(4, 2))
