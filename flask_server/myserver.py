from flask import Flask
from flask import render_template
from flask import request

app = Flask(__name__)

# IMPORTANT POINT: FLASK LOOKS FOR /templates TO SERVE HTML
# render_template is a flask method and needs to be imported first


@app.route('/')
def index():
    return render_template("index.html")


@app.route('/about')
def about():
    return render_template("about.html")


@app.route('/hello')
@app.route('/hello/<name>')
def hello(name=None):
    return render_template('hello.html', name=name)


def valid_login():
    # request.form['username'],
    # request.form['password']
    # # # #
    pass


def log_the_user_in():
    print('user logged in')
    # request.form['username']
    # # # #
    pass


@app.route('/login', methods=['POST', 'GET'])
def login():
    error = None
    if request.method == 'POST':
        if valid_login():
            print('valid login')
            return log_the_user_in()
        else:
            error = 'Invalid username/password'
    # the code below is executed if the request method
    # was GET or the credentials were invalid
    # return render_template('login.html', error=error)
    return render_template('hello.html', name=error)


class get_current_user():
    def __init__(self, name, age):
        self.name = name
        self.age = age


# DEALING WITH JSON
@app.route("/me")
def me_api():
    user = get_current_user('samuca', 30)
    return {
        "username": user.name,
        "age": user.age
    }


if __name__ == "__main__":
    app.run(debug=True)
    # debug=True is set to allow possible errors to appear on the web page.
    # IMPORTANT POINT: This shall never be used in production environment
