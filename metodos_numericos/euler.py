# model function
def f(x, y):
    # return 1+x
    return 1+y


# PVI
y = []
y.insert(0, 1)


# Método de Euler
def euler(h, a, b):
    x = []
    while (a <= b):
        x.append(a)
        a = a + h

    for i in range(len(x)):
        print("\n x={0}, y={1}.\n".format(x[i], y[i]))
        y.append(y[i] + h*f(x[i], y[i]))
    y.pop(len(x))
    # print(len(x), len(y))


euler(0.1, 0, 1)
