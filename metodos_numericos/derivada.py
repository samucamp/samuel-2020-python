# model function
def f(x):
    # return (x)
    return (3*(x**2) + (3*x) + 1)
    # return (1/(1+x))


# forward differences
def derivada1(x, h):
    return (f(x+h) - f(x))/h


# centered differences
def derivada2(x, h):
    return (f(x+h) - f(x-h))/(2*h)


# tests
print(f(2))
print(f(-3))
print(derivada1(2, 0.1))
print(derivada2(2, 0.1))
print(derivada1(2, 0.2))
print(derivada2(2, 0.2))
print(derivada1(2, 0.05))
print(derivada2(2, 0.05))
