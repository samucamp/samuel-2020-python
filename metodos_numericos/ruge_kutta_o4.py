# EDO exemplo
def f(x, y):
    # return -y/(1)
    # return 1+x
    return 1+y


# condição inicial
y = []
# y.insert(0, 10)
y.insert(0, 1)


# Método de Ruge Kutta Ordem 4
def kuta4(h, a, b):
    x = []
    while (a <= b):
        x.append(a)
        a = a + h
    ym = []
    ym.insert(0, y[0])
    for i in range(len(x)-1):
        print("\ni={0}, x={1}, y={2}.\n".format(i, x[i], y[i]))
        k1 = f(x[i], y[i])
        k2 = f(x[i] + (1/2)*h, y[i] + (1/2)*k1*h)
        k3 = f(x[i] + (1/2)*h, y[i] + (1/2)*k2*h)
        k4 = f(x[i] + h, y[i] + k3*h)
        y.append(y[i] + (1/6)*(k1 + 2*k2 + 2*k3 + k4)*h)
    # print(len(x), len(y))


# testes
kuta4(0.1, 0, 1)
