# model function f1
def f1(t, x, y):
    return y


# model function f2
def f2(t, x, y):
    return (9.81-(0.25/68.1)*(y**2))


# valores iniciais
x = [0]
y = [0]


# Método de Euler para sistema de 2 EDO
print("---- Solução para Método de Euler ----\n")


def euler(h, a, b):
    t = []
    while (a <= b):
        t.append(a)
        a = a + h

    for i in range(len(t)):
        print("\n t={0}, x={1}, y={2}.\n".format(t[i], x[i], y[i]))
        x.append(x[i] + h*f1(t[i], x[i], y[i]))
        y.append(y[i] + h*f2(t[i], x[i], y[i]))

    x.pop(len(t))
    y.pop(len(t))


# teste (intervalo h, limite inferior a, limite superior b)
euler(2, 0, 10)
