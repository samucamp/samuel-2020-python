# model function f1
def f1(t, x, y): return y


# model function f2
def f2(t, x, y): return (T/(M-mdot*t)-g)


# condição inicial
x = [0]
y = [0]


# valores
T = 10000
M = 100
mdot = 5
g = 9.8


# Método de Ruge Kutta Ordem 4
def kuta4(h, a, b):
    t = []
    while (a <= b):
        t.append(a)
        a = a + h

    for i in range(len(t)-1):
        print("\ni={0}, t={1}, x={2}, y={3}.\n".format(i, t[i], x[i], y[i]))
        k11 = f1(t[i], x[i], y[i])
        k21 = f2(t[i], x[i], y[i])
        k12 = f1(t[i]+0.5*h, x[i]+0.5*h*k11, y[i]+0.5*h*k21)
        k22 = f2(t[i]+0.5*h, x[i]+0.5*h*k11, y[i]+0.5*h*k21)
        k13 = f1(t[i]+0.5*h, x[i]+0.5*h*k12, y[i]+0.5*h*k22)
        k23 = f2(t[i]+0.5*h, x[i]+0.5*h*k12, y[i]+0.5*h*k22)
        k14 = f1(t[i]+h, x[i]+h*k13, y[i]+h*k23)
        k24 = f2(t[i]+h, x[i]+h*k13, y[i]+h*k23)
        x.append(x[i] + (1/6)*h*(k11+2*k12+2*k13+k14))
        y.append(y[i] + (1/6)*h*(k21+2*k22+2*k23+k24))


# testes
kuta4(2, 0, 10)
