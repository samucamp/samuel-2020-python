# model function
def f(x):
    # return (x)
    return (3*(x**2) + (3*x) + 1)
    # return (1/(1+x))


# Soma de retângulos
def integral1(h, a, b):
    ir = 0
    x = a
    while x <= b:
        ir = ir + f(x)*h
        x = x + h
    return ir


# Soma de trapézios retângulos
def integral2(h, a, b):
    ir = 0
    x = a
    while x <= b:
        ir = ir + 0.5*(f(x)+f(x+h))*h
        x = x + h
    return ir


print(integral1(0.05, 0, 2))
print(integral2(0.1, 0, 2))
