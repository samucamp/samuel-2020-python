# model function f1
def f1(t, x, y): return y


# model function f2
def f2(t, x, y): return (9.81-(0.25/68.1)*(y**2))


# valores iniciais
x = [0]
y = [0]


# Método de Euler Melhorado para sistema de 2 EDO
print("---- Solução para Método de Euler Melhorado ----\n")


def eulerm(h, a, b):
    t = []
    while (a <= b):
        t.append(a)
        a = a + h

    for i in range(len(t)-1):
        print("\ni={0}, t={1}, x={2}, y={3}.\n".
              format(i, t[i], x[i], y[i]))
        k1 = h*f1(t[i], x[i], y[i])
        l1 = h*f2(t[i], x[i], y[i])
        k2 = h*f1(t[i]+h, x[i]+k1, y[i]+l1)
        l2 = h*f2(t[i]+h, x[i]+k1, y[i]+l1)
        x.append(x[i] + (1/2)*(k1+k2))
        y.append(y[i] + (1/2)*(l1+l2))


# teste (intervalo h, limite inferior a, limite superior b)
eulerm(2, 0, 10)
