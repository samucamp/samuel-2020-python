import numpy as np

a = np.arange(10)
print(a)

print(np.median(a))
print(np.average(a))
print(np.mean(a))
print(np.std(a))
print(np.var(a))
print(np.histogram(a))
