import numpy as np

x = np.random.randint(low=0, high=9, size=10)
print(x)

x = np.random.rand(3, 3)
print(x)

# 4D
x = np.random.rand(2,2,2,2)
print(x)