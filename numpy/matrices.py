import numpy as np

# RANDOM MATRIZ 3X3
x = np.empty([3, 3], np.uint8)
print(x)

y = np.eye(3, dtype=np.uint8)
print(y)

# shift by 1 >>>
y = np.eye(5, dtype=np.uint8, k=1)
print(y)

# shift downwards
y = np.eye(5, dtype=np.uint8, k=-1)
print(y)

# MATRIZ IDENTIDADE
x = np.identity(5, dtype=np.uint8)
print(x)

# MATRIZ DE ONES (3d)
x = np.ones((2, 5, 5), dtype=np.int16)
print(x)

# MATRIZ DE ZEROS (3d)
x = np.zeros((2, 5, 5), dtype=np.int16)
print(x)

# CREATE MATRIZ FILL VALUE
x = np.full((3, 3, 3), dtype=np.int16, fill_value=5)
print(x)


x = np.tri(5, 5, k=0, dtype=np.uint16)
print(x)

x = np.tri(5, 5, k=-1, dtype=np.uint16)
print(x)

x = np.ones((5, 5), dtype=np.int16)
print(np.tril(x, k=1))
print(np.tril(x, k=-1))
