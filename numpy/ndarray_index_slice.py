import numpy as np

# VETOR
x = np.array([1, 2, 3], np.int16)
print(x)
print(type(x))
print(x[0])
print(x[1])
# print(x[2])
print(x[-1])

# MATRIZ
x = np.array([[1, 2, 3], [4, 5, 6]], np.int16)
print(x)

# SLICING

# array as all lines from column 0
print(x[:, 0])

# array as all columns from line 0
print(x[0, :])

# 3 DIMENSION
x = np.array([[[1, 2, 3], [4, 5, 6]],
              [[7, 8, 9], [10, 11, 12]]], np.int16)
print(x)
print(x[1, 0, 0])  # matriz 2, linha 1, coluna 1

# TRANSPOSE
print(x.T)
