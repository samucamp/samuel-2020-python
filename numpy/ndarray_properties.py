import numpy as np

# 3 DIMENSION
x = np.array([[[1, 2, 3], [4, 5, 6]],
              [[7, 8, 9], [10, 11, 12]]], np.int16)
print(x)
print(x[1, 0, 0])  # matriz 2, linha 1, coluna 1

# TRANSPOSE
print(x.T)

# PROPERTY
print(x.shape)  # returns total each dimension
print(x.ndim)
print(x.dtype)
print(x.size)
print(x.nbytes)
